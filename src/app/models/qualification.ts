export class Qualification{
    constructor(
        public id: number,
        public score: string,
        public song: number
    ){}
}