export class Song{
    constructor(
        public id: number,
        public name: string,
        public author: string
    ){}
}