export class User{
    constructor(
        public id: number,
        public role: string,
        public name: string,
        public email: string,
        public pass: string
    ){}
}