import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/DefaultBundle/login.component';
import { RegisterComponent } from './components/DefaultBundle/register.component';
import { BaseuserComponent } from "./components/UserBundle/baseuser.component";


import { SongNewComponent } from './components/song.new.component';
import { SongListComponent } from './components/song.list.component';
import { SongEditComponent } from "./components/song.edit.component";
import { ListNewComponent } from "./components/list.new.component";
import { ListEditComponent } from  "./components/list.edit.component";
import { ListShowComponent } from  "./components/list.show.component";
import { QualificationNewComponent} from "./components/qualification.new.component";
import { SongAddComponent} from "./components/song.add.component";


const appRoutes: Routes = [
    {path:'', component: LoginComponent},
    {path:'login',component: LoginComponent},
    {path:'login/:id',component: LoginComponent},//validar este, no se sabe para que sirva
    {path:'register',component: RegisterComponent},
    {path:'myacount',component:BaseuserComponent},

    {path:'song-new',component:SongNewComponent},
    {path:'song-list',component:SongListComponent},
    {path:'edit-song/:id',component: SongEditComponent},
    {path:'list-new',component:ListNewComponent},
    {path:'edit-list/:id',component: ListEditComponent},
    {path:'list/:id',component:ListShowComponent},
    {path:'new-qualification/:id',component:QualificationNewComponent},
    {path:'add-song/:id',component:SongAddComponent},
    {path:'**', component:LoginComponent}
];
export const appRoutingProviders: any[] =[];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);