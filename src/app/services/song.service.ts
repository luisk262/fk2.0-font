import  { Injectable } from '@angular/core';
import { Http, Response,Headers } from '@angular/http';
import "rxjs/add/operator/map";
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class SongService{

    public url: string;

    constructor(private _http: Http){
        this.url = GLOBAL.url;
    }

    new(song_new){
        let json=JSON.stringify(song_new);
        let params ="json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+"/song/new",params,{headers:headers})
            .map(res => res.json());
    }
    list(){
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+'/song/',{headers:headers})
            .map(res => res.json());
    }
    edit(song_edit){
        let json=JSON.stringify(song_edit);
        let params ="json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+'/song/edit',params,{headers:headers})
            .map(res => res.json());
    }
    delete(id){
        let json=JSON.stringify(id);
        let params ='json={"id":"'+id+'"}';
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+'/song/delete',params,{headers:headers})
            .map(res => res.json());
    }
}
