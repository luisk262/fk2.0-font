import  { Injectable } from '@angular/core';
import { Http, Response,Headers,RequestOptions } from '@angular/http';
import "rxjs/add/operator/map";
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class QualificationService{

    public url: string;

    constructor(private _http: Http){
        this.url = GLOBAL.url;
    }

    new(token,new_qualification){
        let json = JSON.stringify(new_qualification);
        let params ="json="+json+"&authorization="+token;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+"/qualification/new",params,{headers:headers})
            .map(res => res.json());
    }
    edit(token,list_edit){
        let json=JSON.stringify(list_edit);
        let params ="json="+json+"&authorization="+token;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+'/lists/edit',params,{headers:headers})
            .map(res => res.json());
    }
}
