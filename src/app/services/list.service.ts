import  { Injectable } from '@angular/core';
import { Http, Response,Headers,RequestOptions } from '@angular/http';
import "rxjs/add/operator/map";
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class ListService{

    public url: string;

    constructor(private _http: Http){
        this.url = GLOBAL.url;
    }

    new(token,list_new){
        let json = JSON.stringify(list_new);
        let params ="json="+json+"&authorization="+token;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+"/lists/new",params,{headers:headers})
            .map(res => res.json());
    }
    list(token){
        let params ="authorization="+token;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+'/lists/',params,{headers:headers})
            .map(res => res.json());
    }
    edit(token,list_edit){
        let json=JSON.stringify(list_edit);
        let params ="json="+json+"&authorization="+token;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+'/lists/edit',params,{headers:headers})
            .map(res => res.json());
    }
    delete(id){
        let params ='json={"id":"'+id+'"}';
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+'/lists/delete',params,{headers:headers})
            .map(res => res.json());
    }
    show(token,id){
        let json=JSON.stringify(id);
        console.log(token);
        let params ='json={"id":"'+id+'"}&authorization='+token;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+'/lists/show',params,{headers:headers})
            .map(res => res.json());
    }
    remove(token,id,idlist){
        let params ='authorization='+token;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+'/lists/'+id+'/'+idlist+'/SongList/remove',params,{headers:headers})
            .map(res => res.json());
    }
    qualifications(token){
        let params ='authorization='+token;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        return this._http.post(this.url+'/qualification/',params,{headers:headers})
            .map(res => res.json());
    }
    songadd(token,id,idSong){
            let params ='authorization='+token;
            let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
            return this._http.post(this.url+'/lists/'+id+'/'+idSong+'/add/song',params,{headers:headers})
                .map(res => res.json());
    }
}
