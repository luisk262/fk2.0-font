import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { routing, appRoutingProviders } from  './app.routing';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/DefaultBundle/login.component';
import { RegisterComponent } from './components/DefaultBundle/register.component';
import { MyacountComponent } from "./components/UserBundle/myacount.component";
import { BaseuserComponent} from "./components/UserBundle/baseuser.component";
import { NewCurriculumComponent } from "./components/UserBundle/newCurriculum.component";


import { SongNewComponent } from  './components/song.new.component';
import { SongListComponent } from "./components/song.list.component";
import { SongEditComponent } from "./components/song.edit.component";
import { ListNewComponent } from "./components/list.new.component";
import { ListEditComponent } from "./components/list.edit.component";
import { ListShowComponent } from  "./components/list.show.component";
import { QualificationNewComponent } from  "./components/qualification.new.component";
import { SongAddComponent }from "./components/song.add.component";
import {NewCurriculumInformationComponent} from "./components/UserBundle/NewCurriculum/information.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    MyacountComponent,
    BaseuserComponent,
    NewCurriculumComponent,
      NewCurriculumInformationComponent,

    SongNewComponent,
    SongListComponent,
    SongEditComponent,
    ListNewComponent,
    ListEditComponent,
    ListShowComponent,
    QualificationNewComponent,
    SongAddComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [
      appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
