import { Component, OnInit , NgModule} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Lists } from '../models/lists';
import { ListService } from '../services/list.service';
import { UserService} from '../services/user.service';

@Component({
    selector: 'edit-list/:id',
    templateUrl: '../views/list.edit.html',
    providers: [UserService,ListService]
})
export class ListEditComponent implements OnInit{
    public title: string;
    public lists: Lists;
    public status;
    public token;


    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _listService:ListService,
        private _userService:UserService
    ){
        this.title = 'Editar Lista';
        this.token = this._userService.getToken();
        this._route.params.forEach((params: Params) => {
            let id = +params['id'];
            if(id!=null){
                this.lists = new Lists(id,'');
            }else{
                this.lists = new Lists(null,'');
            }
        });

    }

    ngOnInit(){
        console.log('El componente song.edit.component ha sido cargado')
        console.log(this.lists);
        }
    onSubmit(){
        this._listService.edit(this.token,this.lists).subscribe(
            response =>{
                this.status= response.status;
                if(response.status!='Success'){
                    this.status="Error";
                }else{

                }
            },
            error =>{
                console.log(<any>error)
            }
        )
    }
}