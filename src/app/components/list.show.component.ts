import {Component, OnInit, NgModule,} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Lists } from '../models/lists';
import { Qualification } from '../models/qualification';
import { ListService } from '../services/list.service';
import {UserService} from "../services/user.service";

@Component({
    selector: 'list-show',
    templateUrl: '../views/list.show.html',
    providers: [ListService,UserService]
})
export class ListShowComponent implements OnInit{
    public title: string;
    public lists: Array<Lists>;
    public qualification: Array<Qualification>;
    public status;
    public token;
    public id;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _listService:ListService,
        private _userService:UserService
    ){
        this.title = 'Modifique su lista de reproduccion';
        this._route.params.forEach((params: Params) => {
            this.id = +params['id'];
        });
    }
    ngOnInit(){
        console.log('El componente list.show.component ha sido cargado');
        this.token = this._userService.getToken();
        this.getQualifications();
        this._route.params.forEach((params: Params) => {
            let id = +params['id'];
            if(id!=null){
                this.getListUser(id);
            }
        });

    }
    getListUser(id){
        this._listService.show(this.token,id).subscribe(
            response =>{
                this.status= response.status;
                if(response.status == 'Success'){
                    this.lists = response.data;
                    console.log(this.lists);
                }
            },
            error =>{
                console.log(<any>error)
            }
        );
    }
    getQualifications(){
        this._listService.qualifications(this.token).subscribe(
            response =>{
                this.status= response.status;
                if(response.status == 'Success'){
                    this.qualification = response.data;
                    console.log(this.qualification);
                }
            },
            error =>{
                console.log(<any>error)
            }
        );
    }
    removeSong(id,idlist){
        console.log("has dado clic en remover cancion ");
        this._listService.remove(this.token,id,idlist).subscribe(
            response =>{
                this.status= response.status;
                if(response.status == 'success'){
                    window.location.href ="/list";
                }
                else {
                    window.location.href ="/list";
                }
            },
            error =>{
                console.log(<any>error)
            }
        );
    }
}