import { Component, OnInit , NgModule} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Song } from '../models/song';
import { SongService } from '../services/song.service';
@Component({
    selector: 'new-song',
    templateUrl: '../views/song.new.html',
    providers: [SongService]
})
export class SongNewComponent implements OnInit{
    public title: string;
    public song: Song;
    public status;


    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _songService:SongService
    ){
        this.title = 'Nueva cancion';
        this.song = new Song(1,'','');
    }

    ngOnInit(){
        console.log('El componente song.new.component ha sido cargado');
        console.log(this.song);
        }
    onSubmit(){
        console.log(this.song);
        this._songService.new(this.song).subscribe(
            response =>{
                this.status= response.status;
                if(response.status!='Success'){
                    this.status="Error";
                }else{
                    this.song = new Song(1,'','');
                }
            },
            error =>{
                console.log(<any>error)
            }
        )
    }
}