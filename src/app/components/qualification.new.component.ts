import { Component, OnInit , NgModule} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Qualification } from '../models/qualification';
import { QualificationService } from '../services/qualification.service';
import {UserService} from "../services/user.service";
@Component({
    selector: 'new-qualification/:id',
    templateUrl: '../views/qualification.new.html',
    providers: [UserService,QualificationService]
})
export class QualificationNewComponent implements OnInit{
    public title: string;
    public qualification: Qualification;
    public status;
    public token;
    public id;


    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _qualificationService:QualificationService,
        private _userService:UserService
    ){
        this.title = 'Nueva calificacion';
        this._route.params.forEach((params: Params) => {
            this.id = +params['id'];
            this.qualification = new Qualification(1,'',this.id);
        });
        this.token = this._userService.getToken();
    }

    ngOnInit(){
        console.log('El componente list.new.component ha sido cargado');
        console.log(this.qualification);
        }
    onSubmit(){
        console.log(this.qualification);
        this._qualificationService.new(this.token,this.qualification).subscribe(
            response =>{
                this.status= response.status;
                if(response.status!='Success'){
                    this.status="Error";
                }else{
                    this.qualification = new Qualification(1,'',null);
                }
            },
            error =>{
                console.log(<any>error)
            }
        )
    }
}