import { Component, OnInit , NgModule} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Song } from '../models/song';
import { SongService } from '../services/song.service';

@Component({
    selector: 'song-list',
    templateUrl: '../views/song.html',
    providers: [SongService]
})
export class SongListComponent implements OnInit{
    public title: string;
    public songs: Array<Song>;
    public status;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _songService:SongService
    ){
        this.title = 'Lista de canciones';
    }
    ngOnInit(){
        console.log('El componente song.list.component ha sido cargado');
        this.getAllSongs();
    }
    getAllSongs(){
        this._songService.list().subscribe(
            response =>{
                this.status= response.status;
                if(response.status == 'Success'){
                    this.songs=response.data;
                    console.log(this.songs);
                }
            },
            error =>{
                console.log(<any>error)
            }
        );
    }
    deleteSong(id){
        console.log("has dado clic en eliminar");
        this._songService.delete(id).subscribe(
            response =>{
                this.status= response.status;
                if(response.status == 'success'){
                    window.location.href ='/song-list';
                }
            },
            error =>{
                console.log(<any>error)
            }
        );
    }
}