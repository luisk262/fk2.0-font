import { Component, OnInit , NgModule} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Song } from '../models/song';
import { SongService } from '../services/song.service';
@Component({
    selector: 'edit-song/:id',
    templateUrl: '../views/song.edit.html',
    providers: [SongService]
})
export class SongEditComponent implements OnInit{
    public title: string;
    public song: Song;
    public status;


    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _songService:SongService
    ){
        this.title = 'Editar cancion';
        this._route.params.forEach((params: Params) => {
            let id = +params['id'];
            if(id!=null){
                this.song = new Song(id,'','');
            }else{
                this.song = new Song(null,'','');
            }
        });

    }

    ngOnInit(){
        console.log('El componente song.edit.component ha sido cargado')
        console.log(this.song);
        }
    onSubmit(){
        this._songService.edit(this.song).subscribe(
            response =>{
                this.status= response.status;
                if(response.status!='Success'){
                    this.status="Error";
                }else{

                }
            },
            error =>{
                console.log(<any>error)
            }
        )
    }
}