import { Component, OnInit , NgModule} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Lists } from '../models/lists';
import { ListService } from '../services/list.service';
import {UserService} from "../services/user.service";
@Component({
    selector: 'new-list',
    templateUrl: '../views/list.new.html',
    providers: [UserService,ListService]
})
export class ListNewComponent implements OnInit{
    public title: string;
    public list: Lists;
    public status;
    public token;


    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _listService:ListService,
        private _userService:UserService
    ){
        this.title = 'Nueva Lista';
        this.list = new Lists(1,'');
        this.token = this._userService.getToken();
    }

    ngOnInit(){
        console.log('El componente list.new.component ha sido cargado');
        console.log(this.list);
        }
    onSubmit(){
        console.log(this.list);
        this._listService.new(this.token,this.list).subscribe(
            response =>{
                this.status= response.status;
                if(response.status!='Success'){
                    this.status="Error";
                }else{
                    this.list = new Lists(1,'');
                }
            },
            error =>{
                console.log(<any>error)
            }
        )
    }
}