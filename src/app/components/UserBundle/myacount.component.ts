import { Component, OnInit , NgModule} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Lists } from '../../models/lists';
import { ListService } from '../../services/list.service';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'myacount',
    templateUrl: '../../views/UserBundle/myaccount.html',
    providers: [UserService,ListService]
})
export class MyacountComponent implements OnInit{
    public title: string;
    public lists: Array<Lists>;
    public status;
    public token;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _listService:ListService,
        private _userService:UserService
    ){
        this.title = 'Myaccount';
        this.token = this._userService.getToken();
    }
    ngOnInit(){
        console.log('El componente myacount.component ha sido cargado');
        this.getAllList();
    }
    getAllList(){
        this._listService.list(this.token).subscribe(
            response =>{
                this.status= response.status;
                if(response.status == 'Success'){
                    this.lists=response.data;
                    console.log(this.lists);
                }
            },
            error =>{
                console.log(<any>error)
            }
        );
    }
    deleteList(id){
        console.log("has dado clic en eliminar");
        this._listService.delete(id).subscribe(
            response =>{
                this.status= response.status;
                if(response.status == 'success'){
                    window.location.href ='/myacount';
                }
            },
            error =>{
                console.log(<any>error)
            }
        );
    }
}