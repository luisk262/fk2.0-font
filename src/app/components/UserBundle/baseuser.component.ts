import { Component, OnInit , NgModule} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Lists } from '../../models/lists';
import { ListService } from '../../services/list.service';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'baseUser',
    templateUrl: '../../views/UserBundle/baseUser.html',
    providers: [UserService,ListService]
})
export class BaseuserComponent implements OnInit{
    public title: string;
    public lists: Array<Lists>;
    public status;
    public token;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _listService:ListService,
        private _userService:UserService
    ){
        this.token = this._userService.getToken();
    }
    ngOnInit(){
        console.log('El componente myacount.component ha sido cargado');
    }
}