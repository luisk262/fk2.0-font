import { Component, OnInit , NgModule} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ListService } from '../../../services/list.service';
import { UserService } from '../../../services/user.service';

@Component({
    selector: 'newExperience',
    templateUrl: '../../../views/UserBundle/NewCurriculum/aptitudes.html',
    providers: [UserService,ListService]
})
export class NewCurriculumExperienceComponent implements OnInit{
    public title: string;
    public status;
    public token;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService:UserService
    ){
        this.title = 'New Curriculum information';
        this.token = this._userService.getToken();
    }
    ngOnInit(){
        console.log('El componente newcurriculum.component ha sido cargado');
    }

}