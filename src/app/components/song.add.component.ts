import { Component, OnInit , NgModule} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SongAdd } from '../models/songadd';
import { ListService } from '../services/list.service';
import {UserService} from "../services/user.service";
import { Song } from "../models/song";
import {SongService} from "../services/song.service";

@Component({
    selector: 'add-song/:id',
    templateUrl: '../views/song.add.html',
    providers: [UserService,ListService,SongService]
})
export class SongAddComponent implements OnInit{
    public title: string;
    public songadd: SongAdd;
    public songs: Array<Song>;
    public status;
    public token;
    public idList;


    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _listService:ListService,
        private _userService:UserService,
        private _songService:SongService
    ){
        this.title = 'Agrege una cancion a su lista';
        this.songadd = new SongAdd(1,'','');
        this.token = this._userService.getToken();
        this._route.params.forEach((params: Params) => {
            this.idList = +params['id'];
        });
    }

    ngOnInit(){
        console.log('El componente song.add.component ha sido cargado');
        console.log(this.songs);
        this.getAllSongs();
        }
    onSubmit(){
        console.log(this.songadd);
        this._listService.songadd(this.token,this.idList,this.songadd.idsong).subscribe(
            response =>{
                this.status= response.status;
                if(response.status!='Success'){
                    this.status="Error";
                }else{
                    this.songadd = new SongAdd(1,'','');
                }
            },
            error =>{
                console.log(<any>error)
            }
        )
    }
    addSong(id){
        this._listService.songadd(this.token,this.idList,id).subscribe(
            response =>{
                this.status= response.status;
                if(response.status!='Success'){
                    this.status="Error";
                }else{
                    this.songadd = new SongAdd(1,'','');
                    window.location.href ="/list";
                }
            },
            error =>{
                console.log(<any>error)
            }
        )
    }
    getAllSongs(){
        this._songService.list().subscribe(
            response =>{
                this.status= response.status;
                if(response.status == 'Success'){
                    this.songs=response.data;
                    console.log(this.songs);
                    console.log("cargado");
                }
            },
            error =>{
                console.log(<any>error)
            }
        );
    }
}