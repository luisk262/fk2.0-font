import { FmyappPage } from './app.po';

describe('fmyapp App', () => {
  let page: FmyappPage;

  beforeEach(() => {
    page = new FmyappPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
